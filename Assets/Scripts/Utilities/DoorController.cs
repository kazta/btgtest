﻿namespace BTGAssemblerTets
{
    using UnityEngine;
    using UnityStandardAssets.Characters.ThirdPerson;
    using UnityEngine.SceneManagement;
    public class DoorController : MonoBehaviour
    {
        bool isActiveDoor = false;
        private void Update()
        {
            if (isActiveDoor)
                transform.Rotate(new Vector3(0, 80, 0));
        }
        private void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player"))
            {
                ThirdPersonCharacter tpc = other.gameObject.GetComponent<ThirdPersonCharacter>();
                Animator anim = tpc.Animator;
                anim.SetFloat("Move", 0);
                tpc.enabled = false;
                other.gameObject.GetComponent<ThirdPersonUserControl>().enabled = false;
                anim.SetTrigger("OpenDoor");
                isActiveDoor = true;
                SceneManager.LoadScene("Exterior",LoadSceneMode.Single);
            }
        }
    }
}

